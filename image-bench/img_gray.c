/**
 * A very basic (and abstracted) image processing filter that 
 * converts an RBG image (represented as 3 matrices) into a
 * grayscale image using the CIE 1931 color space
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define num 256 

void convert(float ** R, float ** G, float ** B, float ** lum) 
{
    // Formula: Y = 0.2126*R + 0.7152*G + 0.0722*B
    for (int y = 0; y < num; y++) {
        for (int x = 0; x < num; x++) {
            //float r = 0.2126*R[y][x];
            //float g = 0.7152*G[y][x];
            //float b = 0.0722*B[y][x];
            //lum[y][x] = r + g + b;
            lum[y][x] = 0.2126*R[y][x] + 0.7152*G[y][x] + 0.0722*B[y][x];
            //lum[y][x] = R[y][x] + G[y][x] + B[y][x]; 
        }
    }
}

int main() {
    float ** R = (float **)malloc(num * sizeof(float *));
    float ** G = (float **)malloc(num * sizeof(float *));
    float ** B = (float **)malloc(num * sizeof(float *));
    float ** lum = (float **)malloc(num * sizeof(float *));

    for (int i = 0; i < num; i++) {
        R[i] = (float *)malloc(num * sizeof(float));
        G[i] = (float *)malloc(num * sizeof(float));
        B[i] = (float *)malloc(num * sizeof(float));
        lum[i] = (float *)malloc(num * sizeof(float));
    }

    for (int y = 0; y < num; y++) {
        for (int x = 0; x < num; x++) {
            R[y][x] = x * 0.2;
            G[y][x] = x * 0.3;
            B[y][x] = x * 0.4;
            lum[y][x] = 0.0;
        }
    }

    convert(R, G, B, lum);
    printf("Success!\n");
    return 0;
}
