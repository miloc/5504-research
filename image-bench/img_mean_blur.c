/**
 * Performs a mean blur on a grayscale image
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define num 256 

void convert(float ** lum, float ** newLum) 
{
    float tmp;
    // Formula: Y = 0.2126*R + 0.7152*G + 0.0722*B
    for (int y = 0; y < num; y++) {
        for (int x = 0; x < num; x++) {
            tmp = 0;
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    if (y + i < 0 || x + j < 0 || y + i >= num || x + j >= num) {
                        tmp += 0;
                    }
                    else {
                        tmp += 1/9 * lum[y+i][x+j];
                    }
                }
            }
            newLum[y][x] = tmp;
        }
    }
}

int main() {
    float ** newLum = (float **)malloc(num * sizeof(float *));
    float ** lum = (float **)malloc(num * sizeof(float *));

    for (int i = 0; i < num; i++) {
        newLum[i] = (float *)malloc(num * sizeof(float));
        lum[i] = (float *)malloc(num * sizeof(float));
    }

    for (int y = 0; y < num; y++) {
        for (int x = 0; x < num; x++) {
            lum[y][x] = 0.3;
            newLum[y][x] = 0;
        }
    }

    convert(lum, newLum);
    printf("Success!\n");
    return 0;
}
