#!/usr/bin/bash

# This shell script will build vectorized and un-vectorized versions of code
# for x86_64 and  AArch64
# Must have the AArch64 cross compiler placed in your home dir under ~/aarch

# Build the x86 with O1 and -ftree-vectorize -fopt-info-vec-all and static

ARM=~/aarch/bin/aarch64-none-linux-gnu-gcc
BASE="-static -O2"
#XM="/usr/lib64/libm.a"
#AM="/home/ugrads/majors/miloc/aarch/aarch64-none-linux-gnu/libc/usr/lib64/libm.a"
XM=""
AM=""
#AM="~/aarch/aarch64-none-linux-gnu/libc/usr/lib64/libm.a"
#echo "Building x86"

#echo "Vectorized"
echo "x86" && gcc $BASE -ftree-vectorize -msse -fopt-info-vec-optimized $1.c -o $1-x86-vec

#echo "No vectorize"
gcc $BASE -fno-tree-vectorize $1.c -o $1-x86-nvec

#echo "Building AArch64"

#echo "Vectorized"
echo "arm" && $ARM $BASE -ftree-vectorize -fopt-info-vec-optimized $1.c -o $1-arm-vec

#echo "No vectorize"
$ARM $BASE -fno-tree-vectorize $1.c -o $1-arm-nvec
