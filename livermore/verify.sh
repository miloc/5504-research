#!/usr/bin/bash
echo "Printing Failures"
for f in *.c
do
    num=$(find -name "${f%.c}*" | wc -l)
    if [ $num -eq 1 ]
    then
        echo $f

        echo $num
    fi
done
