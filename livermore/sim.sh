#!/usr/bin/bash
#for f in *.c
#do
    ../run_sim.sh ${f%.c} &
#done
../run_sim.sh LLoops_1_hydro &
../run_sim.sh LLoops_2_ICCG &
../run_sim.sh LLoops_7_EqnOfState &
../run_sim.sh LLoops_12_firstDifference &
../run_sim.sh LLoops_16_MonteCarlo &
../run_sim.sh LLoops_18_explicitHydro &
wait
echo "Simulation Done"
