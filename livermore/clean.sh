#!/usr/bin/bash
for f in *.c
do
    rm ${f%.c}-x86-vec
    rm ${f%.c}-x86-nvec
    rm ${f%.c}-arm-vec
    rm ${f%.c}-arm-nvec
done
