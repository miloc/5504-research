#include <stdio.h>
//#include <math.h>

/* Type-specifiers for function declarations */
#ifdef CRAY
#define CALLED_FROM_FORTRAN fortran
#define FORTRAN_FUNCTION    fortran
#else
#define CALLED_FROM_FORTRAN
#define FORTRAN_FUNCTION    extern
#endif

/* These are the names to be used for the functions */
//#ifdef CRAY
//#define KERNEL kernel
//#define TRACE  trace
//#define SPACE  space
//#define TEST   test
//#define TRACK  track
//#else
//#define KERNEL kernel_
//#define TRACE  trace_
//#define SPACE  space_
//#define TEST   test_
//#define TRACK  track_
//#endif

/* Type-specifiers for the structs that map to common */
//#ifdef CRAY
//#define COMMON_BLOCK fortran
//#else
#define COMMON_BLOCK 
//#endif

/* Names of structs (or common blocks) */
#ifdef CRAY
#define ALPHA  alpha
#define BETA   beta
#define SPACES spaces
#define SPACER spacer
#define SPACE0 space0
#define SPACEI spacei
#define ISPACE ispace
#define SPACE1 space1
#define SPACE2 space2
#else
#define ALPHA  alpha_
#define BETA   beta_
#define SPACES spaces_
#define SPACER spacer_
#define SPACE0 space0_
#define SPACEI spacei_
#define ISPACE ispace_
#define SPACE1 space1_
#define SPACE2 space2_
#endif

/* Declare the four fortran functions called */
//FORTRAN_FUNCTION TRACE();
//FORTRAN_FUNCTION SPACE();
//FORTRAN_FUNCTION TEST();
//FORTRAN_FUNCTION TRACK();
//
/* Define the structs or COMMON BLOCKS */

COMMON_BLOCK struct {
    long Mk;
    long Ik;
    long Im;
    long Ml;
    long Il;
    long Mruns;
    long Nruns;
    long Jr;
    long Npfs[47][3][8];
} ALPHA ;
#define mk     ALPHA.Mk
#define ik     ALPHA.Ik
#define im     ALPHA.Im
#define ml     ALPHA.Ml
#define il     ALPHA.Il
#define mruns  ALPHA.Mruns;
#define nruns  ALPHA.Nruns;
#define jr     ALPHA.Jr
#define npfs   ALPHA.Npfs

COMMON_BLOCK struct {
    double Tic;
    double Times[47][3][8];
    double See[3][8][3][5];
    double Terrs[47][3][8];
    double Csums[47][3][8];
    double Fopn[47][3][8];
    double Dos[47][3][8];
} BETA ;
#define tic     BETA.Tic
#define times   BETA.Times
#define see     BETA.See
#define terrs   BETA.Terrs
#define csums   BETA.Csums
#define fopn    BETA.Fopn
#define dos     BETA.Dos

COMMON_BLOCK struct {
    long Ion;
    long J5;
    long K2;
    long K3;
    long MULTI;
    long Laps;
    long Loop;
    long M;
    long Kr;
    long It;
    long N13h;
    long Ibuf;
    long Npass;
    long Nfail;
    long N;
    long N1;
    long N2;
    long N13;
    long N213;
    long N813;
    long N14;
    long N16;
    long N416;
    long N21;
    long Nt1;
    long Nt2;
} SPACES ;
#define  ion    SPACES.Ion
#define  j5     SPACES.J5
#define  k2     SPACES.K2
#define  k3     SPACES.K3
#define  multi  SPACES.MULTI
#define  laps   SPACES.Laps
#define  loop   SPACES.Loop
#define  m      SPACES.M
#define  kr     SPACES.Kr
#define  it     SPACES.It
#define  n13h   SPACES.N13h
#define  ibuf   SPACES.Ibuf
#define  npass  SPACES.Npass
#define  nfail  SPACES.Nfail
#define  n      SPACES.N
#define  n1     SPACES.N1
#define  n2     SPACES.N2
#define  n13    SPACES.N13
#define  n213   SPACES.N213
#define  n813   SPACES.N813
#define  n14    SPACES.N14
#define  n16    SPACES.N16
#define  n416   SPACES.N416
#define  n21    SPACES.N21
#define  nt1    SPACES.Nt1
#define  nt2    SPACES.Nt2

COMMON_BLOCK struct {
    double A11;
    double A12;
    double A13;
    double A21;
    double A22;
    double A23;
    double A31;
    double A32;
    double A33;
    double Ar;
    double Br;
    double C0;
    double Cr;
    double Di;
    double Dk;
    double Dm22;
    double Dm23;
    double Dm24;
    double Dm25;
    double Dm26;
    double Dm27;
    double Dm28;
    double Dn;
    double E3;
    double E6;
    double Expmax;
    double Flx;
    double Q;
    double Qa;
    double R;
    double Ri;
    double S;
    double Scale;
    double Sig;
    double Stb5;
    double T;
    double Xnc;
    double Xnei;
    double Xnm;
} SPACER ;
#define  a11     SPACER.A11
#define  a12     SPACER.A12
#define  a13     SPACER.A13
#define  a21     SPACER.A21
#define  a22     SPACER.A22
#define  a23     SPACER.A23
#define  a31     SPACER.A31
#define  a32     SPACER.A32
#define  a33     SPACER.A33
#define  ar      SPACER.Ar
#define  br      SPACER.Br
#define  c0      SPACER.C0
#define  cr      SPACER.Cr
#define  di      SPACER.Di
#define  dk      SPACER.Dk
#define  dm22    SPACER.Dm22
#define  dm23    SPACER.Dm23
#define  dm24    SPACER.Dm24
#define  dm25    SPACER.Dm25
#define  dm26    SPACER.Dm26
#define  dm27    SPACER.Dm27
#define  dm28    SPACER.Dm28
#define  dn      SPACER.Dn
#define  e3      SPACER.E3
#define  e6      SPACER.E6
#define  expmax  SPACER.Expmax
#define  flx     SPACER.Flx
#define  q       SPACER.Q
#define  qa      SPACER.Qa
#define  r       SPACER.R
#define  ri      SPACER.Ri
#define  s       SPACER.S
#define  scale   SPACER.Scale
#define  sig     SPACER.Sig
#define  stb5    SPACER.Stb5
#define  t       SPACER.T
#define  xnc     SPACER.Xnc
#define  xnei    SPACER.Xnei
#define  xnm     SPACER.Xnm

COMMON_BLOCK struct {
    double Time[47];
    double Csum[47];
    double Ww[47];
    double Wt[47];
    double Ticks;
    double Fr[9];
    double Terr1[47];
    double Sumw[7];
    double Start;
    double Skale[47];
    double Bias[47];
    double Ws[95];
    double Total[47];
    double Flopn[47];
    long Iq[7];
    long Npf;
    long Npfs1[47];
} SPACE0 ;
#define  time    SPACE0.Time
#define  csum    SPACE0.Csum
#define  ww      SPACE0.Ww
#define  wt      SPACE0.Wt
#define  ticks   SPACE0.Ticks
#define  fr      SPACE0.Fr
#define  terr1   SPACE0.Terr1
#define  sumw    SPACE0.Sumw
#define  start   SPACE0.Start
#define  skale   SPACE0.Skale
#define  bias    SPACE0.Bias
#define  ws      SPACE0.Ws
#define  total   SPACE0.Total
#define  flopn   SPACE0.Flopn
#define  iq      SPACE0.Iq
#define  npf     SPACE0.Npf
#define  npfs1   SPACE0.Npfs1

COMMON_BLOCK struct {
    double Wtp[3];
    long Mult[3];
    long Ispan[3][47];
    long Ipass[3][47];
} SPACEI ;
#define wtp    SPACEI.Wtp
#define mult   SPACEI.Mult
#define ispan  SPACEI.Ispan
#define ipass  SPACEI.Ipass

COMMON_BLOCK struct {
    long E[96];
    long F[96];
    long Ix[1001];
    long Ir[1001];
    long Zone[300];
} ISPACE ;
#define e    ISPACE.E
#define f    ISPACE.F
#define ix   ISPACE.Ix
#define ir   ISPACE.Ir
#define zone ISPACE.Zone

COMMON_BLOCK struct {
    double U[1001];
    double V[1001];
    double W[1001];
    double X[1001];
    double Y[1001];
    double Z[1001];
    double G[1001];
    double Du1[101];
    double Du2[101];
    double Du3[101];
    double Grd[1001];
    double Dex[1001];
    double Xi[1001];
    double Ex[1001];
    double Ex1[1001];
    double Dex1[1001];
    double Vx[1001];
    double Xx[1001];
    double Rx[1001];
    double Rh[2048];
    double Vsp[101];
    double Vstp[101];
    double Vxne[101];
    double Vxnd[101];
    double Ve3[101];
    double Vlr[101];
    double Vlin[101];
    double B5[101];
    double Plan[300];
    double D[300];
    double Sa[101];
    double Sb[101];
} SPACE1 ;
#define  u    SPACE1.U
#define  v    SPACE1.V
#define  w    SPACE1.W
#define  x    SPACE1.X
#define  y    SPACE1.Y
#define  z    SPACE1.Z
#define  g    SPACE1.G
#define  du1  SPACE1.Du1
#define  du2  SPACE1.Du2
#define  du3  SPACE1.Du3
#define  grd  SPACE1.Grd
#define  dex  SPACE1.Dex
#define  xi   SPACE1.Xi
#define  ex   SPACE1.Ex
#define  ex1  SPACE1.Ex1
#define  dex1 SPACE1.Dex1
#define  vx   SPACE1.Vx
#define  xx   SPACE1.Xx
#define  rx   SPACE1.Rx
#define  rh   SPACE1.Rh
#define  vsp  SPACE1.Vsp
#define  vstp SPACE1.Vstp
#define  vxne SPACE1.Vxne
#define  vxnd SPACE1.Vxnd
#define  ve3  SPACE1.Ve3
#define  vlr  SPACE1.Vlr
#define  vlin SPACE1.Vlin
#define  b5   SPACE1.B5
#define  plan SPACE1.Plan
#define  d    SPACE1.D
#define  sa   SPACE1.Sa
#define  sb   SPACE1.Sb

COMMON_BLOCK struct {
    double P[512][4];
    double Px[101][25];
    double Cx[101][25];
    double Vy[25][101];
    double Vh[7][101];
    double Vf[7][101];
    double Vg[7][101];
    double Vs[7][101];
    double Za[7][101];
    double Zp[7][101];
    double Zq[7][101];
    double Zr[7][101];
    double Zm[7][101];
    double Zb[7][101];
    double Zu[7][101];
    double Zv[7][101];
    double Zz[7][101];
    double B[64][64];
    double C[64][64];
    double H[64][64];
    double U1[2][101][5];
    double U2[2][101][5];
    double U3[2][101][5];
} SPACE2 ;
#define  p       SPACE2.P
#define  px      SPACE2.Px
#define  cx      SPACE2.Cx
#define  vy      SPACE2.Vy
#define  vh      SPACE2.Vh
#define  vf      SPACE2.Vf
#define  vg      SPACE2.Vg
#define  vs      SPACE2.Vs
#define  za      SPACE2.Za
#define  zp      SPACE2.Zp
#define  zq      SPACE2.Zq
#define  zr      SPACE2.Zr
#define  zm      SPACE2.Zm
#define  zb      SPACE2.Zb
#define  zu      SPACE2.Zu
#define  zv      SPACE2.Zv
#define  zz      SPACE2.Zz
#define  b       SPACE2.B
#define  c       SPACE2.C
#define  h       SPACE2.H
#define  u1      SPACE2.U1
#define  u2      SPACE2.U2
#define  u3      SPACE2.U3


