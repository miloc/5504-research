#!/usr/bin/bash

for f in *.c
do
    ../build.sh ${f%.c} &
done

wait

# Verify that we built all

#echo "Printing Failures"
#for f in ./*.c
#do
#    num=$(find -name "./${f%.c}*" | wc -l)
#    if [ $num -eq 1 ]
#    then
#        echo $f
#
#        echo $num
#    fi
#done
