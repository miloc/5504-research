# Simple CPU Config
import m5
from m5.objects import *

m5.util.addToPath("../gem5/configs/")

from caches import *

# Add command line options to the script
from common import SimpleOpts 

thispath = os.path.dirname(os.path.realpath(__file__))
binary = os.path.join(
    thispath,
    #"./matmul.novector",
    #"./matmul86",
    #"./nq86",
    #"./lts86",
    #"./LLoops_1_hydro-arm-vec",
)

def_cpu = "ARM"
SimpleOpts.add_option("binary", nargs="?", default=binary)
SimpleOpts.add_option("cpu", nargs="?", default=def_cpu)


args = SimpleOpts.parse_args()
binary = os.path.join(thispath, args.binary)
#print(binary)
# Create our system
system = System()

system.clk_domain = SrcClockDomain()
system.clk_domain.clock = '1GHz'
system.clk_domain.voltage_domain = VoltageDomain()

system.mem_mode = 'timing'
system.mem_ranges =[AddrRange('8192MB')] # Give plenty of RAM

system.membus = SystemXBar()

if "x" in args.cpu.lower():
    system.cpu = X86TimingSimpleCPU()
    #system.cpu = MinorO3CPU()
else:
    system.cpu = ArmTimingSimpleCPU()
    #system.cpu = MinorO3CPU()

system.cpu.createInterruptController()
system.membus = SystemXBar()
# Specify what CPU we are running
if "x" in args.cpu.lower():
    print("Simulating x86")
#    system.cpu = X86TimingSimpleCPU()
    # x86 only
    system.cpu.interrupts[0].pio = system.membus.mem_side_ports
    system.cpu.interrupts[0].int_requestor = system.membus.cpu_side_ports
    system.cpu.interrupts[0].int_responder = system.membus.mem_side_ports
else:
    print("Simulating ARM")
#    system.cpu = ArmTimingSimpleCPU()
    #system.cpu.createInterruptController()


# Instantiate L1 instruction and data caches
system.cpu.icache = L1ICache()
system.cpu.dcache = L1DCache()

# Connect caches to CPU
system.cpu.icache.connectCPU(system.cpu)
system.cpu.dcache.connectCPU(system.cpu)

# Create a coherenet L2 crossbar
system.l2bus = L2XBar()

# Connect caches to L2 bus 
system.cpu.icache.connectBus(system.l2bus)
system.cpu.dcache.connectBus(system.l2bus)

# Create L2 cache
system.l2cache = L2Cache()
system.l2cache.connectCPUSideBus(system.l2bus)

# Create L3 bus
system.l3bus = L3XBar()

system.l2cache.connectMemSideBus(system.l3bus)
system.l3cache = L3Cache()
system.l3cache.connectCPUSideBus(system.l3bus)


system.l3cache.connectMemSideBus(system.membus)

system.system_port = system.membus.cpu_side_ports

system.mem_ctrl = MemCtrl()
system.mem_ctrl.dram = DDR3_1600_8x8()
system.mem_ctrl.dram.range = system.mem_ranges[0]
system.mem_ctrl.port = system.membus.mem_side_ports

system.workload = SEWorkload.init_compatible(binary)

process = Process()
process.cmd = [binary]
system.cpu.workload = process
system.cpu.createThreads()

root = Root(full_system = False, system = system)
m5.instantiate()

print("Beginning Simulation")
exit_event = m5.simulate()

print('Exiting @ tick {} because {}'.format(m5.curTick(), exit_event.getCause()))

