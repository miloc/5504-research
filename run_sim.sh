#!/usr/bin/bash
# Runner script for the livermore loops
# Need to make sure thta GEM5 points to your gem5 installation
HOME="/home/ugrads/majors/miloc"
GEM5="$HOME/gem5_workspace/gem5"
echo "Running 4 simulations in parallel"

$GEM5/build/X86/gem5.opt -d out/$1/sim-$1-x86-nvec ../simple.py livermore/$1-x86-nvec x86 &
$GEM5/build/X86/gem5.opt -d out/$1/sim-$1-x86-vec ../simple.py livermore/$1-x86-vec x86 &
$GEM5/build/ARM/gem5.opt -d out/$1/sim-$1-arm-nvec ../simple.py livermore/$1-arm-nvec arm &
$GEM5/build/ARM/gem5.opt -d out/$1/sim-$1-arm-vec ../simple.py livermore/$1-arm-vec arm &

wait
echo "Finished simulation :)"


