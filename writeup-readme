# Readme for Milo and Davis' final 5504 project

This project is a simulation based project comparing the performance of vector instructions in 
AArch64 and x86_64.

We include the writeup as a pdf in the top directory.
In the writeup is a link to the git repo, but we also include the code in the directory.

To re-create the results there are multiple steps.
First you must build all the test programs.
Go into the image-bench directory and run build.sh.
Then go into the livermore directory and run build_l.sh
This will build all the test files.

To run the simulation for the livermore loops run the sim.sh script in the livermore
directory.
This will place the results in the out directrory.
The copy.sh script then moves them all to the res directory, which will contain
the same data as the livermore-results directory.

For the image benchmarks, you will have to run each individually by using the rs.sh
script in the main directory.
Invoke the script with the base name of the test and it will run all the simulation
for x86, arm and vector and non-vector.

The git repository for the project can be found here:
    https://git.cs.vt.edu/miloc/5504-research
